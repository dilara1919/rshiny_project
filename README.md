# Shiny Application
This shiny application is created for choosing compatible sequencing 
indices. The selection of compatible indices when sequencing with different 
libraries is important. The sequencer has to be able to read each index and assign each read to its original sample. 
If the indices are not compatible, the sequencer is not able to assign the reads or read an index completely.
This tool will help choose these indices.

## Input files
The available indices are going to be selected from samplesheet and index files. 
The files have to follow the structure from the [Illumina website](https://emea.support.illumina.com/downloads/sample-sheet-v2-template.html).

## Set up and Installations 
### First step
Before using the application. The repository has to be cloned.
```
git clone https://gitlab.com/dilara1919/rshiny_project
```
For more information see [github](https://docs.github.com/en/github/creating-cloning-and-archiving-repositories).
### Second step 
The application will be executed with a docker. 
The docker can be downloaded for following operating systems: Windows, OS and Linux. 
For download and installation instructions please follow the [docker documentation](https://docs.docker.com/desktop/).

## Execution
Before execution the Dockerfile make sure you are in the path of the Dockerfile. 
The Dockerfile can be executed via terminal. With the following commands:
```
docker build -t <selected_name_for_docker> .
docker run -d --rm -p 3838:3838  <selected_name_for_docker>
```
The docker will install all dependencies listed in the Dockerfile. This might take 15-20 minutes. 
If the installation process is finished, the application can be accessed with your browser. Open your browser of 
choice and access your localhost as following: 
```
http://localhost:3838/
```

## Output files
The selected indices can be downloaded and will be displayed in a yaml file. 
For more information about the output and the use of the application you can read the introduction page of 
the shiny application. 

## Authors
Dilara Damar, Ashkan Ghassemi, Simon Tausch
