library(shiny)
library(shinythemes)   # appearance of app
library(reticulate)    # to import python code 
library(rhandsontable) # table usage
library(stringr)       # split string
library(ggplot2)       # creating plots 
library(tidyr)         # use of gather
library(dplyr)         # use of mutate
library(bioseq)        # build reverse complement of indices 


# define inputs and outputs + layout of shiny app
ui <- navbarPage(
  title = "Expresso bar",
  theme = shinytheme("flatly"),
  
  # let's get started with app
  tabPanel("Calculate index sets",
  # left site of shiny app -> user inputs
  sidebarLayout(
    sidebarPanel(
      id="sidebar",

    # all inputs for shiny app are defined
    h4("Inputs for selection of indices"),
    br(),
    fileInput(inputId = "samplesheet", label = "Select your predefined Samplesheets. Use CTRL+Klick to select multiple files.",accept = c('.csv'), multiple = TRUE),  # with accept we set input type of files
    uiOutput("Select"),
    # kit
    fileInput(inputId = "index_files", label = "Select your candidate Index files. Use CTRL+Klick to select multiple files.",accept = c('.tsv'), multiple  = TRUE),
    # threshold
    sliderInput(inputId = "threshold", label = "Select distance threshold", min = 1,  max = 5, value = 2),
    # checkbox for samplesheet
    checkboxInput("reverse_index", label = "Click if you upload a reverse complement samplesheet", value = FALSE),
    # checkbox for ILP
    h4("Inputs for calculation of compatible indices"),
    checkboxInput("break_ilp", label = "Break calculation if indices from samplesheet collide. Otherwise, incompatible indices from the predefined samplesheet are removed", value = FALSE)
    ),


    # the right site of web-page we will see the user outputs
    mainPanel(

      tabsetPanel(
        id="panel_reset",
        # output kits
        tabPanel("Kits",
                 br(),
                 uiOutput("Select_Kit"),
                 numericInput(inputId = "index_number", label = "Number of indices to select from kit", value = 5, min = 1, max = 384),
                 br(),
                 rHandsontableOutput(outputId = "table_kit"),
                 br(),
                 rHandsontableOutput(outputId = "distance_kit"),
                 br(),
                 h4("Summary"),
                 p("This summary will display your selected input for the uploaded index files: How many indices will be chosen from each kit and the threshold."),
                 htmlOutput("summary2"),
                 rHandsontableOutput(outputId = "summary_table"),
                 br(),
                 htmlOutput("summary"),
                 br(),
                 # actionButton(inputId = "reset", label = "Reset Selection"),
                 # downloadButton(outputId="downloadData2", label="Download"),
        ),


      # output samplesheets
      tabPanel("Samplesheets",
               br(),
               dataTableOutput(outputId = "checkbox"),
               rHandsontableOutput(outputId = "set_table"),
               br(),
               rHandsontableOutput(outputId = "distance_samplesheet"),
               br(),
               # downloadButton(outputId="downloadData", label="Download"),
               br(),
               p("With the click of the start collision calculation button, the program will calculate possible collisions between indices in a samplesheet. The results can be seen in the summary.
                 The indices will be listed in a table below with the corresponding samplename. In order to know which indices in a specific file are incompatible,
                  you can have a look into the last column."),
               p("Remark: Make sure the table is not filled with whitespaces when creating a table of your own. This process might take a while."),
               br(),
               actionButton(inputId = "collision_button", label = "Start collision calculation"),
               h3("Summary"),
               htmlOutput("collosion"),
               span(htmlOutput("collision2"), style = "color:red"),
               br(),
               rHandsontableOutput("collision_table"),
               br(),
               htmlOutput("collision3"),
               br(),
               downloadButton(outputId = "downloadData3", label = "Selected inputs"),
               ),


      # contamination Outputs
      # tabPanel("Contamination",
      #          br(),
      # ),


      # output ILP
      tabPanel("Calculation of compatible index sets",
              h3("Description"),
              p(("Now that you have selected your indices, you can calculate a maximal independent set from your indices. You have the option to
              break the process if any indices collide. In order to do that check the box under inputs for calculation of compatible indices. The results will display an index set
              which is maximal. Remark: The process depends on several
                 inputs. Make sure you checked your inputs. Once the process starts you can not quit the calculation.")),
              p(("The results can be downloaded as a fasta file.")),
              br(),
              actionButton("button", "Start process"),
              br(),
              br(),
              htmlOutput("ilp"),
              br(),
              htmlOutput("ilp2"),
              htmlOutput("ilp3"),
              br(),
              htmlOutput("base_matrix_description"),
              br(),
              plotOutput("base_matrix", inline = TRUE),
              br(),
              br(),
              downloadButton(outputId = "downloadData_ilp", label = "Download results"),

              )
          )



        )
      )

    ),

  # add a Readme for user
  navbarMenu(
    "Help",
    tabPanel("Introduction",
             mainPanel(
               div(
                 strong(h1("Introduction"))
               ),
               p(h4("This app is created for choosing compatible sequencing indices for mixing different libraries on Illumina Sequencing devices.")),
               p(h4("The selection of compatible incides when sequencing with different libraries is very important. The sequencer has to be able to read each index
                    and assign each read to its sample of origin. If index sequences do not have a sufficient distance, they are incompatible. It is then impossible to assign the reads reliably via demultiplexing, leading to intra-run contaminations.")),
               p(h4("Expresso bar will help you choosing compatible sets of indices from different libraries, also when the number of indices per read or their length differ. ")),
               p(h4("To download test data,  please klick ", tags$a(href="https://gitlab.com/bfr_bioinformatics/expresso-bar/-/archive/shinyapps_deploy_version/expresso-bar-shinyapps_deploy_version.zip?path=example_data", "here", target="_blank"), ". Then upload the downloaded files in the tab Calculate index sets.")),
               p(h4("To use the application, please open the tab Calculate index sets above. "))
             )
    ),
    tabPanel("Inputs & Outputs",
             mainPanel(
               div(
                 strong(h1("Inputs")),
                 p(h4(
                   HTML(
                     paste0(
                       "The indices that are going to be selected from samplesheet and index files. The samplesheets have to follow the structure as described in
                        the ", tags$a(href="https://emea.support.illumina.com/downloads/sample-sheet-v2-template.html", "Illumina template"), "."
                     )))),
                 p(h4("The index files have to be tsv file that follows a similiar structure as the samplesheets.
                            The index files contain the indices from one specific kit. The user can choose the number of indices for each kit.
                            Important you can only combine one samplesheet with mulitple index files. To do so, please upload all required index files at once, selecting them with CTRL+Klick in the upload dialogue.")),
                 p(h4("If you want to combine multiple indices from different samplesheets or sets, you have the option to create a table from scratch or edit an existing table.
                          While doing so there will be a column called info, in which the set has to be named.")),
               ),
               p(h4("You can also choose a threshold. The threshold will define the minimal distance between the indices.")),
               br(),
               div(
                 strong(h1("Outputs")),
                 p(h4("The selected indices can be downloaded and viewed from a yaml file. The structure of the yaml file can be seen in the example bellow. ")),
                 img(src='structure_example.PNG', align = "center"),
                 p(h4("The taken indices will contain the indices from different samplesheets or one samplesheet and to_select will posses the indices from different kits."))
               ),
               br(),
             )
    )
  )
  
  )
  


  
  





