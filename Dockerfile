
## exixting docker with important installed shiny dependencies
FROM rocker/shiny:4.0.0

## update system libraries and install miniconda
RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get clean
RUN apt-get install python3-venv -y
RUN apt-get install -y libpython3-dev


## copy all important files into new directory called app
COPY server.R /app/
COPY ui.R /app/
COPY script /app/script
COPY www/ /app/www


## install all important packages necessary for executing shiny app (in R)
RUN Rscript -e 'install.packages("remotes")'
RUN Rscript -e 'remotes::install_github("fkeck/bioseq")'
RUN Rscript -e 'install.packages("reticulate")'
RUN Rscript -e 'install.packages("rhandsontable")'
RUN Rscript -e 'install.packages("stringr")'
RUN Rscript -e 'install.packages("shinythemes")'
RUN Rscript -e 'install.packages("ggplot2")'
RUN Rscript -e 'install.packages("tidyr")'
RUN Rscript -e 'install.packages("dplyr")'
# RUN Rscript -e 'install.packages("bioseq")'
# RUN Rscript -e 'install.packages("BiocManager")'
# RUN Rscript -e 'BiocManager::install("beadarray")'


## create an environment to use reticulate + install mip vor ILP
RUN Rscript -e 'reticulate::install_miniconda(path = reticulate::miniconda_path(), update = TRUE, force = FALSE)'
RUN Rscript -e 'reticulate::conda_create("r-reticulate")'
RUN Rscript -e 'reticulate::use_condaenv("r-reticulate")'
# use the environment
RUN Rscript -e "reticulate::conda_install(packages ='mip', pip = TRUE)"

## install all dependencies used in python script for execution in R
RUN Rscript -e 'reticulate::conda_install(packages = "pandas")'
RUN Rscript -e 'reticulate::conda_install(packages = "sample-sheet", pip = TRUE)'
RUN Rscript -e 'reticulate::conda_install(packages = "numpy")'
RUN Rscript -e 'reticulate::conda_install(packages = "mergedeep")'
RUN Rscript -e 'reticulate::conda_install(packages = "pyyaml")'
# RUN Rscript -e 'reticulate::py_install("xlrd")'
# RUN Rscript -e 'reticulate::py_install("json")'
# RUN Rscript -e 'reticulate::py_install("python-Levenshtein")'
# import the python script used in R-shiny app


## load python script from directory
RUN Rscript -e 'reticulate::source_python("/app/script/readdata.py")'
RUN Rscript -e 'reticulate::source_python("/app/script/work_revisited_main_mac.py")'


## define port, can include mulitple port with expose, than access them with
## docker run -p <host_port>:<container_port>
EXPOSE 3838


## command for executing shiny app
CMD ["R", "-e", "shiny::runApp('/app/', host = '0.0.0.0', port = 3838)"]




# notes
# Dockerfile:
# installation docker over website
# installation all dependencies hier already shiny/rocker- docker which include installation of r-base (can be changed)
# then others python etc..

# commands:
# build the image: sudo docker build -t myshiny-docker-test(can be any name) .
# run the image on docker with port:  sudo docker run -d --rm -p 3838:3838 myshiny-docker-test
